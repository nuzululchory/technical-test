package id.co.dian.karyawan.service;

import java.util.List;

import id.co.dian.karyawan.dto.GroupDto;
import id.co.dian.karyawan.model.Group;

public interface GroupService {

	Group getById(Integer idGroup);
	
	List<Group> findAll();
	
	Group save(GroupDto group);
}
