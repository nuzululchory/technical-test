package id.co.dian.karyawan.service;

import java.util.List;

import id.co.dian.karyawan.model.Employee;

public interface EmployeeService {
	List<Employee> findAll();

	List<Employee> getAktif();

	Employee save(Employee param);

	Employee checkUnique(Employee param);

	Employee getByNip(Integer number);

	void delete(Integer id);
}
