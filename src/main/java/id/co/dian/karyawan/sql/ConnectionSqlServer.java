package id.co.dian.karyawan.sql;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

public class ConnectionSqlServer {
	@Autowired
	private DataSource dataSource;

	public void connect() {
		try {
			this.connection = dataSource.getConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		try {
			if (this.connection == null)
				this.connect();
			else if (this.connection.isClosed())
				this.connect();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return this.connection;
	}

	public Connection connection = null;
	public ResultSet rs = null;
	public CallableStatement cl = null;
	public PreparedStatement pr = null;
}
