package id.co.dian.karyawan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CustomizedBadRequestException extends RuntimeException {
	
	public CustomizedBadRequestException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}
